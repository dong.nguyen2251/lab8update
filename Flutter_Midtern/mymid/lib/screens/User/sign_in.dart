import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../root_page.dart';




class SigninScreen extends StatefulWidget{
  const SigninScreen({Key?key}) :super(key:key);

  @override
  _SigninScreenState createState() => _SigninScreenState();
  
 
}



class _SigninScreenState extends State<SigninScreen>{

  final _formKey = GlobalKey<FormState>();
  //editing
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  //firebase
  final _auth = FirebaseAuth.instance;


  
  @override
  Widget build(BuildContext context) {
    //email
    final emailField = TextFormField(
      autofocus:false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      validator: (value)
      {
        if(value !.isEmpty)
        {
          return ("Please enter your email");
        }
        if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
        .hasMatch(value))
        {
          return ("Please enter correct email");
        }
        return null;
      },
      onSaved: (value){
        emailController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.mail),
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    ); 
    //Password
    final passwordField = TextFormField(
      
      autofocus:false,
      controller: passwordController,
      obscureText: true,
      
      validator: (value){
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return ("Password is required for login");
        }
        if(!regex.hasMatch(value)){
          return ("Please enter correct Password(Min. 6 Character)");
        }
      },
      onSaved: (value){
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.vpn_key),
        
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Password",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        fillColor: Colors.blue
      ),

      
    ); 
    final loginButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.blueAccent,
      child: MaterialButton(
        onPressed: (){
          signIn(emailController.text, passwordController.text);
        },
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,



        child: Text("Đăng nhập",
        style: TextStyle(fontSize:20,fontWeight:FontWeight.bold,color: Colors.white),
        textAlign: TextAlign.center,
        ),),
    );
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color:Colors.grey), 
          onPressed: () { 
            Navigator.of(context).pop();
           },),
        ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.black,
            child:Padding(
              padding: const EdgeInsets.all(36.0),

            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                
                children: <Widget>[
                 
                  SizedBox(
                    height: 200,
                    
                    
                  
                  ),
                   Text("zoom",style: TextStyle(color: Colors.blueAccent,fontSize: 45,fontWeight: FontWeight.bold),),
                  SizedBox(
                    height: 45,
                  ),
                  emailField,
                  SizedBox(
                    height: 25,
                  ),
                  passwordField,
                  SizedBox(
                    height: 35,
                  ),
                  loginButton,
                  SizedBox(
                    height: 15,
                  ),
                  /*Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Don't have an account?"),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> RegisterScreen()));
                        },
                        child: Text(" Sign Up",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 15,color: Colors.blueAccent),),
                      )
                    ],
                  )*/
                ],
              ),)
            ),
           ),)),
    );
  } 
  void signIn(String email,String password) async
  {
    if(_formKey.currentState!.validate())
    {
      await _auth.signInWithEmailAndPassword(email: email, password: password)
      .then((uid) => {
        Fluttertoast.showToast(msg: "Login Successful"),
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>RootPage()))
      }).catchError((e){
        Fluttertoast.showToast(msg: e!.message);
      });
    }
  }
}
