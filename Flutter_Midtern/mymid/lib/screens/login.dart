import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mymid/screens/User/sign_in.dart';
import 'package:mymid/constants/constants.dart';
import 'package:mymid/screens/User/sign_in.dart';
import 'package:mymid/screens/User/sign_up.dart';

import '../widgets/custom_button.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  
  



  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
       backgroundColor: Colors.black,
       leading: IconButton(
         
         icon: Icon(Icons.settings),
         onPressed: (){

         },
         ),
       actions: []),
       body: SafeArea(
         
        child: Column(
          
           mainAxisAlignment: MainAxisAlignment.center,
           children: [
           const Text(
             'Bắt đầu',
             style: const TextStyle(
               fontSize: 24,
               fontWeight: FontWeight.bold),
           ),
           Padding(
             padding: const EdgeInsets.symmetric(vertical:38.0),
             child: Image.asset("assets/images/computer.jpg"),
           ),
           CustomButton(
             text: 'Tham gia cuộc họp ', 
             onPressed: (){
               Navigator.push(context, MaterialPageRoute(builder: (context)=> SigninScreen()));
             }
             ),
             Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Padding(
                   padding: const EdgeInsets.all(20),
                   child: GestureDetector(
                     onTap: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=> RegisterScreen()));
                     },
                     child: Text("Đăng kí",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 15,color: Colors.blueAccent)),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(20),
                   child: GestureDetector(
                     
                     onTap: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=> SigninScreen()));
                     },
                     child: Text("Đăng nhập",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 15,color: Colors.blueAccent)),
                   ),
                 )
               ],
             ),
         ],
         
       ),
    )
    );
  }
}
