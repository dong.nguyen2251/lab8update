import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mymid/utils/colors.dart';
import 'package:mymid/widgets/home_meeting_button.dart';
import 'package:mymid/widgets/others_meeting_button.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _page = 0;
  onPageChanged(int page){
    setState(() {
      _page = page;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: backgroundColor,
        //elevation: 0,
        title: const Text('Gặp gỡ và trò chuyện'),
        centerTitle: true,
        actions: []),
        body: Column(
          
          children: [
           
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                HomeMeetingButton(
                  onPressed: (){},
                  text:'Cuộc họp mới',
                  icon: Icons.videocam,
                  
                ),
                OtherMeetingButton(
                  onPressed: (){}, 
                  icon: Icons.add_box_rounded, 
                  text: 'Tham gia'
                  ),
                OtherMeetingButton(
                  onPressed: (){}, 
                  icon: Icons.calendar_today, 
                  text: 'Lên lịch'
                  ),
                OtherMeetingButton(
                  onPressed: (){}, 
                  icon: Icons.arrow_upward_rounded, 
                  text: 'Chia sẻ màn hình'
                  ),  
              ],
            ),
            const Expanded(
              child: Center(
                child: Text('Tìm người và bắt đầu cuộc trò chuyện!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18),),
              ),
              ),
          ],
        ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: footerColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        onTap: onPageChanged,
        currentIndex: _page,
        type: BottomNavigationBarType.fixed,
        unselectedFontSize: 14,

        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.comment_bank,),
            label: 'Gặp gỡ và trò chuyện',
          ),

          BottomNavigationBarItem(
            
            icon: Icon(Icons.lock_clock,),
            label: 'Cuộc họp',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Liên lạc',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.settings_outlined,),
            label: 'Khác',
          ),  
        ]),
    );
  }
}