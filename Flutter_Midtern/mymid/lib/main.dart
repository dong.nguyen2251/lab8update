import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mymid/splash_screen/splash_screen.dart';

Future<void> main() async { 
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    
  );
  runApp(MyApp()); 
  }
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: Colors.black,
        
      ),
     
      home: SplashScreenPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

